import { NextApiRequest, NextApiResponse } from 'next';

const handler = (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method === 'GET') {
    const data = {
      type: 'sunny',
      degrees: 70,
    };
    res.status(200).send(data);
  }
};

export default handler;
