// Global
import type { NextPage } from 'next';
import fs from 'fs';
import path from 'path';
// Lib
import fetchPost, { PostData } from '../../lib/fetch-post';
import translateDate from '../../lib/translate-date';

interface PostPageParams {
  post: string;
}

export const getStaticPaths = async () => {
  const SAMPLE_POSTS_PATH = path.join(
    __dirname,
    '../../../../public/sample-posts'
  );
  const fileNames = fs.readdirSync(SAMPLE_POSTS_PATH);
  // Trim the file extension, and format to match the expected output
  const paths: { params: PostPageParams }[] = fileNames.map((file) => ({
    params: { post: file.replace(/\.[^/.]+$/, '') },
  }));
  console.log(paths);

  return {
    paths: paths,
    fallback: false,
  };
};

export const getStaticProps = async ({
  params,
}: {
  params: PostPageParams;
}) => {
  const postData = await fetchPost(params.post);

  if (!!postData) {
    return {
      props: {
        ...postData,
      },
    };
  }

  return {
    notFound: true,
  };
};

const Post: NextPage<PostData> = ({ title, description, date, copy }) => {
  return (
    <div className="max-w-prose mx-auto py-12">
      <h1 className="text-3xl font-bold leading-tight mb-1">{title}</h1>
      <p className="text-lg mb-2">{description}</p>
      <p className="text-xs mb-12">{translateDate(date)}</p>
      <div>
        {copy.map((paragraph, i) => (
          <p key={i} className="mb-4">
            {paragraph}
          </p>
        ))}
      </div>
    </div>
  );
};

export default Post;
