import type { NextPage } from 'next';

const About: NextPage = () => {
  return (
    <div>
      The only prerequisite is that it makes you happy. If it makes you happy
      then it's good. This is where you take out all your hostilities and
      frustrations. It's better than kicking the puppy dog around and all that
      so. Everything is happy if you choose to make it that way. You gotta think
      like a tree. We must be quiet, soft and gentle.
    </div>
  );
};

export default About;
