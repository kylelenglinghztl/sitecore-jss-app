// Global
import Image from 'next/image';
import Link from 'next/link';

interface MyCardProps {
  title: string;
  description: string;
  link: {
    href: string;
    text: string;
  };
  image?: {
    src: string;
    alt: string;
    height: string;
    width: string;
  };
}

const MyCard = ({
  title,
  description,
  link,
  image,
}: MyCardProps): JSX.Element => {
  return (
    <div className="border border-b-4 border-gray rounded-md flex flex-col">
      {!!image && <Image {...image} />}
      <div className="p-4 flex flex-col flex-1 justify-between">
        <div className="mb-8">
          <h2 className="font-black text-2xl mb-1">{title}</h2>
          <p>{description}</p>
        </div>
        <Link href={link.href}>
          <a>{link.text}</a>
        </Link>
      </div>
    </div>
  );
};

export default MyCard;
