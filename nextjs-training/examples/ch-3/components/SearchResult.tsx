// Interfaces
import type { MockSearchResult } from '../third-party/mock-api';
// Lib
import translateDate from '../lib/translate-date';

const SearchResult = ({
  title,
  author,
  date,
  description,
}: MockSearchResult): JSX.Element => (
  <li className="p-4 border">
    <p className="text-lg font-bold">{title}</p>
    <p className="text-sm italic">
      {author} • {translateDate(date)}
    </p>
    <p>{description}</p>
  </li>
);

export default SearchResult;
