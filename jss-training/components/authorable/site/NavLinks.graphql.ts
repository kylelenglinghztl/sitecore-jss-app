import { gql } from '@apollo/client';

const NavLinks = gql`
  fragment navLinks on C__NavigationLink {
    navigationLinkUrl {
      jsonValue
    }
  }
`;

export default NavLinks;
