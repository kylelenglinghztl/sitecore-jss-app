// Lib
import { getSampleRenderingContext } from 'lib/mocks/mock-placeholder';

const defaultData = {
  rendering: {
    ...getSampleRenderingContext('hztl-layout-page-section'),
    componentName: 'LayoutPageSection',
  },
  fields: {
    sectionTheme: {
      fields: {
        Value: {
          value: 'theme-inherit',
        },
      },
    },
  },
};

export default defaultData;
