const IconClose = (): JSX.Element => (
  <path
    d="M40.617 12.333L28.95 24l11.667 11.667-4.95 4.95L24 28.95 12.333 40.617l-4.95-4.95L19.05 24 7.383 12.333l4.95-4.95L24 19.05 35.667 7.383l4.95 4.95z"
    fill="currentColor"
  />
);

export default IconClose;
