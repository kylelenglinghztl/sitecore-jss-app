const IconMinus = (): JSX.Element => (
  <path d="M4,20.5 L44,20.5 L44,27.5 L4,27.5 L4,20.5 z" fill="currentColor" />
);

export default IconMinus;
