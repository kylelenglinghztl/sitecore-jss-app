const IconHamburger = (): JSX.Element => (
  <path
    d="m4 37.667h40v-4.445h-40zm0-11.111h40v-4.446h-40v4.445zm0-15.556v4.444h40v-4.444z"
    fill="currentColor"
  />
);

export default IconHamburger;
