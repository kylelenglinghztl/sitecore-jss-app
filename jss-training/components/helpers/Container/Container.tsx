interface ContainerProps {
  children: React.ReactNode | React.ReactNode[];
  dataComponent?: string;
}

const Container = ({ children, dataComponent }: ContainerProps): JSX.Element => {
  return (
    <div className="mx-auto w-full max-w-screen-xl">
      <div className="px-4 md:px-8 lg:px-12" data-component={dataComponent}>
        {children}
      </div>
    </div>
  );
};

export default Container;
