import classNames from 'classnames';

export type Theme = 'theme-black' | 'theme-white' | 'theme-green' | 'theme-inherit';

export const getThemeClasses = (theme: Theme | undefined): string =>
  theme === 'theme-inherit' || !theme ? '' : classNames(theme, 'bg-theme-bg', 'text-theme-text');
