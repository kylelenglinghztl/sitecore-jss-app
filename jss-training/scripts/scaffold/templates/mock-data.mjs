// Utils
import { getImportString } from '../get-import-string.mjs';

const kebabCase = (str) =>
  str
    .replace(/([a-z])([A-Z])/g, '$1-$2')
    .replace(/[\s_]+/g, '-')
    .toLowerCase();

export const mockData = ({ name, hasGetStaticProps, hasPlaceholder }) => {
  /**
   * Imports
   */
  const imports = {
    components: [],
    global: [],
    lib: [],
    local: [],
    test: [],
  };

  if (hasPlaceholder) {
    imports.lib.push(`import { getSampleRenderingContext } from 'lib/mocks/mock-placeholder';`);
  }

  /**
   * UID
   */
  const UID = hasGetStaticProps
    ? `const UID = '${kebabCase(name)}-uid';
`
    : '';

  /**
   * Rendering Data
   */
  let renderingData = '';

  if (hasPlaceholder) {
    renderingData += `...getSampleRenderingContext('placeholder-name'),
`;
  }

  if (hasGetStaticProps) {
    renderingData += `uid: UID,
`;
  }

  const rendering = !!renderingData
    ? `
  rendering: {
    ${renderingData}componentName: '${name}',
  },
`
    : '';

  /**
   * Static Props Dictionary
   */
  const staticProps = hasGetStaticProps
    ? `export const staticPropsData = {
  [UID]: {},
};

`
    : '';

  /**
   * Output
   */
  return `${getImportString(imports)}${UID}const defaultData = {${rendering}};
  
${staticProps}export default defaultData;
`;
};
