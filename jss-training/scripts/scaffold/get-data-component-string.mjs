export const getDataComponentString = (name, subdirectory) =>
  `${subdirectory.dataComponent}${name.toLowerCase()}`;
