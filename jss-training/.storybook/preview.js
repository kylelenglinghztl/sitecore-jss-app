// Global
import classNames from 'classnames';
// Lib
import { getThemeClasses } from '../lib/get-theme';
// Global stylesheet for components.
import '../styles/globals.css';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

export const globalTypes = {
  theme: {
    name: 'Theme',
    description: 'Theme',
    defaultValue: 'theme-black',
    toolbar: {
      icon: 'paintbrush',
      items: [
        { value: 'theme-black', title: 'Black' },
        { value: 'theme-green', title: 'Green' },
        { value: 'theme-white', title: 'White' },
      ],
    },
  },
  padding: {
    name: 'Padding',
    description: 'Viewport padding',
    defaultValue: 'On',
    toolbar: {
      icon: 'switchalt',
      items: ['On', 'Off'],
    },
  },
};

export const decorators = [
  (Story, context) => (
    <div
      className={classNames(getThemeClasses(context.globals.theme), {
        ['min-h-screen']: context.viewMode === 'story',
        ['px-gutter-all py-4']: context.globals.padding === 'On',
        ['px-gutter-all']: context.globals.padding === 'Off',
      })}
    >
      {Story()}
    </div>
  ),
];
