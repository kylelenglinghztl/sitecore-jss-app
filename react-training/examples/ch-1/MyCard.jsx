const MyCard = ({ title, description, link, image }) => {
  return (
    <div>
      {!!image && <img src={image.src} alt={image.alt} />}
      <h2>{title}</h2>
      <p>{description}</p>
      <a href={link.href}>{link.text}</a>
    </div>
  );
};

export default MyCard;
