import { useState, useEffect, useRef } from 'react';
import classNames from 'classnames';

const MyCollapse = ({ title, content }) => {
  const [isOpen, setIsOpen] = useState(false);

  const collapse = useRef(null);

  const handleClick = () => {
    setIsOpen(!isOpen);
  };

  useEffect(() => {
    // Set up our click handler
    const handleClickOutside = (event) => {
      if (!collapse.current.contains(event.target)) {
        setIsOpen(false);
      }
    };

    // Make sure collapse.current isn't null still
    if (!!collapse.current) {
      // Add the listener when we open, take it away when we close
      if (isOpen) {
        window.addEventListener('click', handleClickOutside);
      } else {
        window.removeEventListener('click', handleClickOutside);
      }
    }

    // If this component is destroyed, remove that event listener
    return () => {
      window.removeEventListener('click', handleClickOutside);
    };
  }, [isOpen, collapse]);

  const containerClasses = [
    'scale-y-0',
    'overflow-hidden',
    'transition',
    'transition-transform',
    'origin-top',
  ];

  if (isOpen) {
    containerClasses.push('scale-y-100');
  }

  return (
    <div ref={collapse} className="max-w-prose mx-auto">
      <button onClick={handleClick}>{title}</button>
      <div
        className={classNames(
          'scale-y-0',
          'overflow-hidden',
          'transition',
          'transition-transform',
          'origin-top',
          { 'scale-y-100': isOpen }
        )}
      >
        <p>{content}</p>
      </div>
    </div>
  );
};

export default MyCollapse;
