import MyLink from './MyLink';

const MyCard = ({ title, description, link, image }) => {
  return (
    <div className="border border-b-4 border-gray rounded-md flex flex-col">
      {!!image && <img src={image.src} alt={image.alt} />}
      <div className="p-4 flex flex-col flex-1 justify-between">
        <div className="mb-8">
          <h2 className="font-black text-2xl mb-1">{title}</h2>
          <p>{description}</p>
        </div>
        <MyLink {...link} variant="button" />
      </div>
    </div>
  );
};

export default MyCard;
