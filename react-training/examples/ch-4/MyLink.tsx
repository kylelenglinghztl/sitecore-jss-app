type MyLinkVariant = 'link' | 'button';

interface MyLinkProps {
  href: string;
  text: string;
  variant?: MyLinkVariant;
}

const LINK_VARIANTS: Record<MyLinkVariant, string> = {
  link: 'font-bold text-turquoise hover:underline',
  button:
    'bg-rose text-black p-2 font-bold rounded-md text-center border-0 border-b-2 border-rose-dark hover:bg-rose-dark hover:underline',
};

const MyLink = ({ href, text, variant = 'link' }: MyLinkProps): JSX.Element => {
  return (
    <a href={href} className={LINK_VARIANTS[variant]}>
      {text}
    </a>
  );
};

export default MyLink;
