# React Training

[React for JSS](https://horizontal.atlassian.net/wiki/spaces/HFEOS/pages/7273404432450/React+for+JSS)

This is the practice space for our crash course on React.

## Setup

```
npm install
npm start
```

Open your browser to `http://localhost:3000`. If you haven't completed any chapters it will be a completely blank page. Perfect for you to fill with your brilliant learnings.
